const assert = require('chai').assert
const currentWeather = require('../src/server.js')


dataMock = {
  'current': {
    'temp_c': 29,
    'humidity': 33,
    'condition': {
      'text': 'Soleado'
    }
  }
}

it("should return json object", () => {
  assert.deepEqual(currentWeather(dataMock), {
    'temp': '29 °C',
    'humedad': '33%',
    'condicionActual': 'Soleado'
  })
})

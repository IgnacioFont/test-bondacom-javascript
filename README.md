# Test Bondacom Javascript

### **Consigna**
Con el fin de implementar una nueva funcionalidad en el dashboard de los usuarios, se pide la
construcción de un pequeño servicio que cuente con dos endpoints que permitan obtener la
siguiente información:
1. El clima actual de la ciudad de Buenos Aires.
2. La temperatura mínima y la máxima de las últimas 24hs. Para ello, se deberá almacenar la
información y calcular dichos datos.

Para cumplir con dicho requerimiento, se utilizará la API http://api.apixu.com/v1.
En la autenticación se solicitará un token de identificación. (Si no lo recibió, consulte con el
entrevistador).

### **Consideraciones**
* Debe retornarse la temperatura en grados celsius, humedad y la condición del dia en texto.
* La respuesta deberá traducirse al castellano.
* El método de almacenamiento de la información (Para el segundo endpoint) queda a criterio
(gustos y preferencias) del desarrollador.
* El nombre de las rutas queda a consideración del desarrollador.

const express = require('express')
const fetch = require('node-fetch')
const mongodb = require('mongodb').MongoClient

const url = 'mongodb://localhost:27017/bondacom-weather-db'
const app = express()
const port = 3000

const currentWeatherAPI = 'https://api.apixu.com/v1/current.json?key=eb3d643539d747aa8a810850182602&q=Argentina&lang=es'


const currentWeather = data => {

  return ({
  'temp': `${data.current.temp_c} °C`,
  'humedad': `${data.current.humidity}%`,
  'condicionActual': `${data.current.condition.text}`})

}

const getMinTemp = dbo =>
dbo.collection('temperatures').find()
.sort({'temperature': 1}).limit(1).toArray()

const getMaxTemp = dbo =>
dbo.collection('temperatures').find()
.sort({'temperature': -1}).limit(1).toArray()

app.get('/api/currentWeather', (req, res) => {
  fetch(currentWeatherAPI)
    .then(r => r.json())
    .then(data => res.json(currentWeather(data)))
    .catch(err => res.send(err))
})

app.get('/api/minMaxTemperatures', (req, res) => {
  mongodb.connect(url, (err, db) => {
    const dbo = db.db('bondacom-weather-db')
    Promise.all([getMinTemp(dbo), getMaxTemp(dbo)])
    .then(values => res.json(
      {'tempMin': `${values[0][0].temperature} °C`,
       'tempMax': `${values[1][0].temperature} °C`}))
    .then(() => db.close())
  })
})

setInterval(() => {
  fetch(currentWeatherAPI)
  .then(r => r.json())
  .then(data => {
    mongodb.connect(url, (err, db) => {
      if (err) throw err
      const dbo = db.db('bondacom-weather-db')
      dbo.collection('temperatures').insertOne({ 'temperature': data.current.temp_c, 'time': Math.floor(Date.now() / 1000)})
      .then(() => dbo.collection('temperatures').count())
      .then(minutes => {
        const oneDay = 1440
        if (minutes === oneDay) dbo.collection('temperatures').deleteOne()
      })
      .then(() => db.close())
    })
  })
  .catch(e => console.log(e))
}, 60000)

app.listen(port, () => {
  console.log(`server started on port ${port}`)
})

module.exports = currentWeather
